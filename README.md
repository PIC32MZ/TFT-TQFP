## Serial graphics terminal based on TQFT format PIC32

Project contains schematics and PCB layouts for a version of the
serial braphics terminal using a PIC32 in a TQFT format.  Current
leaning is toward a PIC32MZ with a 64 or 100 pin package.

Possible candidates are:

* PIC32MZ0512EFE064
* PIC32MZ0512EFF064
* PIC32MZ0512EFK064
* PIC32MZ1024EFE064
* PIC32MZ1024EFF064
* PIC32MZ1024EFG064
* PIC32MZ1024EFH064
* PIC32MZ1024EFK064
* PIC32MZ1024EFM064
* PIC32MZ2048EFG064
* PIC32MZ2048EFH064
* PIC32MZ2048EFM064

These parts appear to be almost identical with very minor differences.

The `ChipKIT` folder contains a schematic for a shield to test the TFT
on a PIC32MX795F512L.
